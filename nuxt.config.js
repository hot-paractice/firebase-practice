module.exports = {
  mode: 'spa',
  build: {
    vendor: [
      'nuxt-property-decorator'
    ]
  },
  modules: [
    '~/modules/typescript'
  ]
}
